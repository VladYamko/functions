const getSum = (str1, str2) => {
	if (
		(typeof str1 !== 'string' && typeof str2 !== 'string') ||
		(str1.length > 0 && parseInt(str1).toString() !== str1) ||
		(str2.length > 0 && parseInt(str2).toString() !== str2)
	) {
		return false;
	}
	str1 = parseInt(str1) || 0;
	str2 = parseInt(str2) || 0;

	return (str1 + str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
	let postCounter = 0;
	let commentsCounter = 0;

	listOfPosts.map((el) => {
		if (el.author === authorName) {
			postCounter++;
		}
		if (el.comments) {
			for (const coma of el.comments) {
				if (coma.author === authorName) {
					commentsCounter++;
				}
			}
		}
	});

	return `Post:${postCounter},comments:${commentsCounter}`;
};

const tickets = (people) => {
	const count = (element) => {
		let change = element - ticket;
		if (change <= houseMoney) {
			houseMoney += element - change;
			return true;
		}
		return false;
	};
	let houseMoney = 0;
	const ticket = 25;
	return people.every(count) ? 'YES' : 'NO';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
